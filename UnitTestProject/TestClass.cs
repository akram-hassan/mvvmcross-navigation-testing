﻿using CrossTest.Core.Services;
using CrossTest.Core.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvvmCross;
using MvvmCross.Base;
using MvvmCross.Core;
using MvvmCross.Navigation;
using MvvmCross.Tests;
using MvvmCross.ViewModels;
using MvvmCross.Views;

namespace UnitTestProject
{

    [TestClass]
    public class TestClass : MvxIoCSupportingTest
    {
        protected MockDispatcher MockDispatcher
        {
            get;
            private set;
        }

        protected IMvxNavigationService MockNavService
        {
            get;
            private set;
        }

        protected override void AdditionalSetup()
        {
            MockServcie theMock = new MockServcie();
            Ioc.RegisterSingleton<IService>(theMock);

            // for navigation
            MockDispatcher = new MockDispatcher();
            Ioc.RegisterSingleton<IMvxViewDispatcher>(MockDispatcher);
            Ioc.RegisterSingleton<IMvxMainThreadDispatcher>(MockDispatcher);

            // for navigation parsing
            Ioc.RegisterSingleton<IMvxStringToTypeParser>(new MvxStringToTypeParser());
        }

        [TestMethod]
        public void TestServices()
        {
            base.Setup();
            var message = Ioc.Resolve<IService>().SayHello("Koki");
            Assert.AreEqual("Hi Koki, This is hello From Mock", message);
        }

        [TestMethod]
        public void TestCommandNavigationWithMockDispatcher_NoNavigationService()
        {
            //The MockDispatcher doesn't compensate the navigation service, so this isn't working
            //It complains about the missing IMvxNavigationService

            /*
             Following this scenario with the newer API
            https://github.com/MvvmCross/NPlus1DaysOfMvvmCross/blob/master/N-29-TipCalcTest/TipCalcTest.Tests/FirstViewModelTests.cs
             */
            base.Setup();

            Assert.AreEqual(0, MockDispatcher.Requests.Count);

            var firstVM = new Page1ViewModel();
            firstVM.GoToPage1Command.Execute();

            Assert.AreEqual(1, MockDispatcher.Requests.Count);
        }

        [TestMethod]
        public void TestCommandNavigationWithMockDispatcher_EmptyMockNavigationService()
        {
            // The assertion below fails, 
            // the empty mock navigation service doesn't update the dispatcher request count 
            // Doesn't work as expected.

            /*
             Following this scenario with the newer API
            https://github.com/MvvmCross/NPlus1DaysOfMvvmCross/blob/master/N-29-TipCalcTest/TipCalcTest.Tests/FirstViewModelTests.cs
             */
            base.Setup();
            
            Ioc.RegisterSingleton<IMvxNavigationService>(new Mock<IMvxNavigationService>().Object);

            Assert.AreEqual(0, MockDispatcher.Requests.Count);
            var firstVM = new Page1ViewModel();
            firstVM.GoToPage1Command.Execute();
            Assert.AreEqual(1, MockDispatcher.Requests.Count);
        }


        [TestMethod]
        public void TestCommandNavigationWithMockDispatcher_NonEmptyMockNavigationService()
        {
            // The assertion below fails, 
            // the empty mock navigation service doesn't update the dispatcher request count 
            // Doesn't work as expected.

            /*
             Following this scenario with the newer API
            https://github.com/MvvmCross/NPlus1DaysOfMvvmCross/blob/master/N-29-TipCalcTest/TipCalcTest.Tests/FirstViewModelTests.cs
             */
            base.Setup();

            RegisterNonEmptyNavService();

            Assert.AreEqual(0, MockDispatcher.Requests.Count);
            var firstVM = new Page1ViewModel();
            firstVM.GoToPage1Command.Execute();
            Assert.AreEqual(1, MockDispatcher.Requests.Count);
        }

        private void RegisterNonEmptyNavService()
        {
            var mockCollection = new Mock<IMvxViewModelLocatorCollection>();
            var loader = new MvxViewModelLoader(mockCollection.Object);
            MockNavService = new MvxNavigationService(null, loader);

            Ioc.RegisterSingleton<IMvxNavigationService>(new Mock<IMvxNavigationService>().Object);
        }
    }
}