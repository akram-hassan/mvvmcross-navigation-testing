﻿using MvvmCross;
using MvvmCross.ViewModels;
using MvvmCross.Navigation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using MvvmCross.Commands;

namespace CrossTest.Core.ViewModels
{
    public class Page1ViewModel: MvxViewModel
    {
        private MvxCommand command;

        public Page1ViewModel()
        {

            command = new MvxCommand(() => {
                Mvx.IoCProvider.Resolve<IMvxNavigationService>().Navigate<Page3ViewModel>();
                ;
            });
        }

        public string PageName { get; set; } = "Hola";

        public IMvxCommand GoToPage1Command => this.command;
    }
}
