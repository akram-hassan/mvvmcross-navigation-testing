﻿using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CrossTest.Core.ViewModels
{
    public class Page2ViewModel: MvxViewModel
    {
        public string Name { get; set; } = "bola";
        public int VisitorCount { get; set; }
    }
}
