﻿namespace CrossTest.Core.Services
{
    public class Service : IService
    {
        public string SayHello(string name)
        {
            return $"Hello: {name}";
        }
    }
}