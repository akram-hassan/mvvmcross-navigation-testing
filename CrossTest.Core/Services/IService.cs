﻿namespace CrossTest.Core.Services
{
    public interface IService
    {
        string SayHello(string name);
    }
}