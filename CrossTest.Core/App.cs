﻿using CrossTest.Core.Services;
using CrossTest.Core.ViewModels;
using MvvmCross;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CrossTest.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            Mvx.IoCProvider.RegisterType<IService, Service>();

            RegisterAppStart<Page1ViewModel>();
        }
    }
}
